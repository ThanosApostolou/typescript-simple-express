
export default class App {
    x: number;
    y: number;
    constructor() {
        this.x=0;
        this.y=0;
    }

    getProperties(): {x: number; y: number} {
        const obj = {
            x: this.x,
            y: this.y
        };
        return obj;
    }
}